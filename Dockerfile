FROM amazonlinux:2 as build

ARG EKSCTL_VERSION="0.19.0-rc.0"
ARG KUBECTL_VERSION="1.18.2"

RUN yum -y -q install unzip groff less tar gzip \
    && cd /tmp \
    # Install AWS CLI
    && curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    && unzip -q awscliv2.zip \
    && ./aws/install \
    # Install eksctl
    && curl --silent --location "https://github.com/weaveworks/eksctl/releases/download/${EKSCTL_VERSION}/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp \
    && mv /tmp/eksctl /usr/local/bin \
    # Install kubctl
    && curl --silent -LO "https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl" \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl \
    && rm -rf /aws 

# Install Docker CE CLI
RUN amazon-linux-extras enable docker \
    && yum -y -q install docker \
    # Install Docker Compose
    && curl -sSL "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose \
    && chmod +x /usr/local/bin/docker-compose

ENV KUBECONFIG="/.kube/config"

COPY alias/* /usr/bin/
RUN cd /usr/bin && chmod +x ks dc dk

ENTRYPOINT [ "/bin/bash" ]
